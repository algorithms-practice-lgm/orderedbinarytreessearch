// FindInOrderedBinaryTree.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include <iostream>

using namespace std;

class Node
{
public:
    int value;
    Node* Left = NULL;
    Node* Right = NULL;

    Node(int key)
    {
        value = key;
    }

    //! This method returns a pointer to the node with the given​​​​​​‌​​‌​​‌‌‌‌‌‌​​‌​​​‌​​‌​‌​ value.
    Node* find(int v)
    {
        if (v == value)
        {
            return this;
        }

        if (v < value)
        {
            if (!Left)
            {
                return NULL;
            }

            return Left->find(v);
        }

        if (!Right)
        {
            return NULL;
        }
        return Right->find(v);
    };
};

int main()
{
    Node* small = new Node(9);
    small->Left = new Node(7);
    small->Right = new Node(13);

    small->Left->Left = new Node(5);
    small->Left->Right = new Node(8);

    small->Left->Left->Left = new Node(2);
    small->Left->Left->Right = new Node(6);

    small->Right->Right = new Node(17);
    small->Right->Right->Left = new Node(16);


    Node* n1 = small->find(6);
    if (n1)
    {
        cout << "Find returns " << n1->value << endl;
    }
    else
    {
        cout << "Not found" << endl;
    }
}

// Ejecutar programa: Ctrl + F5 o menú Depurar > Iniciar sin depurar
// Depurar programa: F5 o menú Depurar > Iniciar depuración

// Sugerencias para primeros pasos: 1. Use la ventana del Explorador de soluciones para agregar y administrar archivos
//   2. Use la ventana de Team Explorer para conectar con el control de código fuente
//   3. Use la ventana de salida para ver la salida de compilación y otros mensajes
//   4. Use la ventana Lista de errores para ver los errores
//   5. Vaya a Proyecto > Agregar nuevo elemento para crear nuevos archivos de código, o a Proyecto > Agregar elemento existente para agregar archivos de código existentes al proyecto
//   6. En el futuro, para volver a abrir este proyecto, vaya a Archivo > Abrir > Proyecto y seleccione el archivo .sln
